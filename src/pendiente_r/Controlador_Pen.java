package pendiente_r;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador_Pen implements ActionListener {

    private Moddelo_Pen pendiente;
    private Vista_Pen Vist;
    public int paso;

    public Controlador_Pen(Moddelo_Pen pendiente, Vista_Pen Vist) {
        this.pendiente = pendiente;
        this.Vist = Vist;
    }

    public void Arrancarrr() {
        Vist.setTitle("----Mini Pendiente----");
        Vist.getTitulo().setText("Valor de -Y- punto 2 ");
        Vist.setLocationRelativeTo(null);
        Vist.setVisible(true);
        Maestro();
    }

    public void Maestro() {
        Vist.getBotoncalcular().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Vist.getBotoncalcular()) {
            paso++;
            guardarvalores();
            Vist.getValores().setText("");
        }

    }

    public void guardarvalores() {
        switch (paso) {
            case 1:
                Vist.getResultado().setText("----");
                pendiente.setY2(capturar_num());
                Vist.getTitulo().setText("Valor de -X- punto 2 ");
                break;

            case 2:
                pendiente.setX2(capturar_num());
                Vist.getTitulo().setText("Valor de -Y- punto 1 ");
                break;

            case 3:
                pendiente.setY(capturar_num());
                Vist.getTitulo().setText("Valor de -X- punto 1 ");
                break;

            case 4:
                paso = 0;
                pendiente.setX(capturar_num());
                Vist.getResultado().setText("Resultado :" + pendiente.Formula_Pendiente());
                Vist.getTitulo().setText("Valor de -Y- punto 2 ");
                System.out.println("Punato 1 ");
                System.out.println(" X " + pendiente.getX());
                System.out.println("Y " + pendiente.getY());
                System.out.println("Punato 2 ");
                System.out.println("X2 " + pendiente.getX2());
                System.out.println("Y2 " + pendiente.getY2());
                break;
        }
    }

    public float capturar_num() {
        float nume1 = Float.parseFloat(Vist.getValores().getText());
        return nume1;
    }

}
