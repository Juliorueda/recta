package pendiente_r;
public class Moddelo_Pen {

    private float x, x2, y, y2;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getX2() {
        return x2;
    }

    public void setX2(float x2) {
        this.x2 = x2;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getY2() {
        return y2;
    }

    public void setY2(float y2) {
        this.y2 = y2;
    }

    public float Formula_Pendiente() {
        float total = (y2 - y) / (x2 - x);
        return total;
    }

}
